ActionMailer::Base.smtp_settings = {
  address:              "smptp.gmail.com",
  port:                 "587",
  enable_starttls_auto: true,
  user_name:            ENV["email_username"],
  password:             ENV["email_username"],
  authentication:       :plain
}
