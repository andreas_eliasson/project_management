class CommentMailer < ActionMailer::Base
  default from: "no-reply@project-management.com"


  def notify_discussion_owner(comment)
    @comment = comment
    @comment_user_id = @comment.user_id
    @comment_owner = User.find(@comment_user_id)
    @discussion = @comment.discussion
    @discussion_owner_id = @discussion.user_id
    @discussion_owner = User.find(@discussion_owner_id)
    #@url = /projects/"#{discussion.project}"
    mail(to: @discussion_owner.email, subject: "New comment on your discussion")
  end


end
