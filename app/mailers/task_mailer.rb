class TaskMailer < ActionMailer::Base
  default from: "no-reply@project-management.com"

  def notify_task_owner(task)
    @task = task
    @task_owner_id = @task.user_id
    @task_owner = User.find(@task_owner_id)
    @project = @task.project
    @project_owner_id = @project.user_id
    @project_owner = User.find(@project_owner_id)

    mail(to: @project_owner.email, subject: "Your task has been completed")
  end
end
