class Comment < ActiveRecord::Base
  belongs_to :discussion
  belongs_to :user
  belongs_to :user

  validates :body, presence: {message: "must be provided"}
end
