class Project < ActiveRecord::Base

  has_many :discussions, dependent: :destroy
  has_many :tasks, dependent: :destroy
  belongs_to :user

  has_many :members, dependent: :destroy
  has_many :users, through: :members

  has_many :project_tags, dependent: :destroy
  has_many :tags, through: :project_tags

  has_many :favorites, dependent: :destroy
  has_many :favorite_users, through: :favorites, source: :user
  
  validates :title, presence: {message: "must be provided"}
  validates :description, presence: {message: "must be provided"}


end
