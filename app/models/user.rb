class User < ActiveRecord::Base
  
  has_many :projects, dependent: :destroy
  has_many :disussions, dependent: :destroy
  has_many :comments, dependent: :destroy
  has_many :tasks, dependent: :destroy

  has_many :members, dependent: :destroy
  has_many :project_members, through: :members, source: :project

  has_many :favorites, dependent: :destroy
  has_many :favorite_projects, through: :favorites, source: :project


  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable, :validatable



  def name_display 
    if first_name || last_name
      "#{first_name}".squeeze(" ").strip.capitalize + " " + "#{last_name}".squeeze(" ").strip.capitalize
    end
  end

end
