class Task < ActiveRecord::Base
  belongs_to :project
  belongs_to :user

  validates :body, presence: {message: "must be provided"}
  

end
