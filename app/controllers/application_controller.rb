class ApplicationController < ActionController::Base
  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  protect_from_forgery with: :exception

  before_action :configure_devise_permitted_params, if: :devise_controller?

  #after_action :redirect_to_new_question, if: :devise_controller?, only: [:create]


  private

  def configure_devise_permitted_params
    devise_parameter_sanitizer.for(:sign_up) << [:first_name, :last_name]
    devise_parameter_sanitizer.for(:account_update) << [:first_name, :last_name]
  end

  def redirect_to_new_question
    redirect_to new_project_path
  end
  
end


