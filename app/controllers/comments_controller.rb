class CommentsController < ApplicationController

  before_action :authenticate_user!, except: [:index, :show]

  def create 

    @discussion = Discussion.find(params[:discussion_id])
    @comment = @discussion.comments.new(params.require(:comment).permit(:body))
    @comment.user = current_user
    respond_to do |format|
      if @comment.save
        if @comment.user != @discussion.user
          #CommentMailer.notify_discussion_owner(@comment).deliver
          format.html { redirect_to @discussion.project, notice: "Comment created successfully" }
          format.js   { render }
        else
          format.html { redirect_to @discussion.project, alert: "Couldn't create comment" }
          format.js   { render }
        end
      end
    end
  end

  def edit 

    @discussion = Discussion.find(params[:discussion_id])
    @comment = Comment.find(params[:id])
    respond_to do |format|
      format.js { render }
    end
  end  

  def update

    @discussion = Discussion.find(params[:discussion_id])
    @comment = @discussion.comments.find(params[:id]) 
    respond_to do |format|
      if @comment.update_attributes(params.require(:comment).permit(:body))
        format.html { redirect_to @discussion.project, notice: "Comment updated successfully" }
        format.js { render }
      else
        format.js { render }
        flash.now[:alert] = "Oops, something went wrong"
      end   
    end
  end


  def destroy
    @discussion = Discussion.find(params[:discussion_id])
    @comment = @discussion.comments.find(params[:id])
    @comment.destroy
    respond_to do |format|
      format.html { redirect_to @discussion.project, notice: "Comment deleted successfully" }
      format.js { render }
    end
  end

end
