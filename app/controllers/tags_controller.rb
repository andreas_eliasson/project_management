class TagsController < ApplicationController

  before_action :authenticate_user!

  before_action :find_project, only: [:create, :destroy]

  def create
    @project = Project.find(params[:project_id])
    @tag = @project.tags.new(params.require(:tag).permit(:name))
    respond_to do |format|
      if @tag.save
        @project_tag = @project.project_tags.new
        @project_tag.tag_id = @tag.id
        if @project_tag.save
          format.html { redirect_to @project, notice: "Tag created successfully" }
          format.js { render }
        else
          format.html { redirect_to @project, alert: "Couldn't create tag" }
          format.js { render }
        end
      else
        format.html { redirect_to @project, alert: "Couldn't create tag" }
        format.js { render }
      end
    end
  end

  def destroy
    @tag = @project.tags.find(params[:id])
    respond_to do |format|
      if @tag.destroy
        format.html { redirect_to @project, notice: "Tag deleted" }
        format.js { render }
      else
        format.html { redirect_to @project, alert: "Couldn't delete tag" }
      end
    end
  end

  private

    def find_project
      @project = Project.find(params[:project_id])
    end


end
