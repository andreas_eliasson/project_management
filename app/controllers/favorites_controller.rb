class FavoritesController < ApplicationController

  before_action :authenticate_user!

  before_action :find_project, only: [:create, :destroy]

  def create  
    @project = Project.find(params[:project_id])
    @favorite = @project.favorites.new
    @favorite.user = current_user
    if @favorite.save 
      redirect_to @project, notice: "I love this project"
    else
      redirect_to @project, alert: "Couldn't favorite this project"
    end 
  end

  def destroy
    @favorite = @project.favorites.find(params[:id])
    if @favorite.destroy
      redirect_to @project, notice: "Unfavorited"
    else
      redirect_to @project, alert: "couldn't unfavorite"
    end
  end

  def index

    @user = current_user
    @favorites = @user.favorites

  end

  private

  def find_project
    @project = Project.find(params[:project_id])
  end

end


