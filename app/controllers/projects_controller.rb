class ProjectsController < ApplicationController

  before_action :authenticate_user!, except: [:index, :show]

  before_action :find_project, only: [:show, :edit, :update, :destroy]

  def index

    @projects = Project.all
    @project = Project.new

  end

  def new

    @project = Project.new

  end

  def create

    @project = Project.new(params.require(:project).permit(:title, :description))
    @project.user = current_user
    respond_to do |format|
      if @project.save
        format.html { redirect_to projects_path, notice: "Project created successfully" }
        format.js { render }
      else
        format.html { redirect_to projects_path, alert: "Couldn't create project" }
        format.js { render }
        flash.now[:alert] = "Couldn't create project"
      end
    end
  end

  def show

    @discussion = Discussion.new
    @comment = Comment.new
    @task = Task.new
    @ordered_list = @project.tasks.order("action DESC")
    @members = @project.members
    @member = @project.members.where(user: current_user).first
    @tags = @project.tags
    @favorite = @project.favorites.where(user: current_user).first

  end

  def edit
    respond_to do |format|
      format.js { render }
    end
  end

  def update
    @favorite = @project.favorites.where(user: current_user).first
    respond_to do |format|
      if @project.update_attributes(params.require(:project).permit(:title, :description))
        format.html { redirect_to projects_path }
        format.js { render }
      else
        format.js { render }
      end
    end
  end

  def destroy

    @project.destroy

    redirect_to projects_path, notice: "Project deleleted successfully"

  end


  private

  def find_project
    @project = Project.find(params[:id])
  end

end
