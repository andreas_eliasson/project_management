class TasksController < ApplicationController

before_action :authenticate_user!, except: [:index, :show]

def create 

  @project = Project.find(params[:project_id])
  @task = @project.tasks.new(params.require(:task).permit(:body, :action))
  @task.user = current_user
  respond_to do |format|
    if @task.save
      format.html {redirect_to @project, notice: "Task was successfully created"}
      format.js { render } #render create.js.haml
    else
      format.html { redirect_to @project, alert: "Something went wrong, please try again" }
      format.js { render }
      flash.now[:alert] = "Couldn't create task"
    end
  end
end


def destroy

  @project = Project.find(params[:project_id])
  @task = @project.tasks.find(params[:id])
  @task.destroy 
  respond_to do |format|
    format.html {redirect_to @project, notice: "Task was successfully deleted"}
    format.js { render }
  end
end

def edit

  @project = Project.find(params[:project_id])
  @task = @project.tasks.find(params[:id])
  respond_to do |format|
    format.js { render }
  end
end

def update

  @project = Project.find(params[:project_id])
  @task = @project.tasks.find(params[:id])
  respond_to do |format|
    if @task.update_attributes(params.require(:task).permit(:body, :action))
      if @task.user != @project.user
        #TaskMailer.notify_task_owner(@task).deliver
      end
      format.html {redirect_to @project, notice: "Task was successfully updated"}
      format.js { render }
    else
      format.js { render }
      flash.now[:alert] = "Please use form to update"
    end
  end
end
end
