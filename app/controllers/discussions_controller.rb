class DiscussionsController < ApplicationController

  before_action :authenticate_user!, except: [:index, :show]

  def create

    @project = Project.find(params[:project_id])
    @discussion = @project.discussions.new(params.require(:discussion).permit(:title, :body))
    @discussion.user = current_user
    respond_to do |format|
      if @discussion.save
        format.html {redirect_to @project, notice: "Discussion was created successfully"}
        format.js { render }
      else
        format.html { redirect_to @project, alert: "Couldn't create discussion" }
        format.js { render }
      end
    end
  end

  def destroy
    @project = Project.find(params[:project_id])
    @discussion = @project.discussions.find(params[:id])
    @discussion.destroy
    respond_to do |format|
      format.html {redirect_to @project, notice: "Discussion was successfully deleted"}
      format.js { render }
    end
  end

  def show
    @project = Project.find(params[:project_id])
    @discussion = @project.discussions.find(params[:id])
  end
end
