class MembersController < ApplicationController

  before_action :authenticate_user!

  before_action :find_project

  def create
    @member = @project.members.new
    @member.user_id = current_user.id
    respond_to do |format|
      if @member.save
        format.html { redirect_to @project, notice: "Member added successfully" }
        format.js { render }
      else
        format.html { redirect_to @project, alert: "Couldn't save member" }
        format.js { render }
      end
    end
  end

  def destroy
    @member = @project.members.find(params[:id])
    respond_to do |format|
      if @member.destroy
        format.html { redirect_to @project, notice: "Member deleted successfully" }
        format.js { render }
      else
        redirect_to @project, alert: "Couldn't delete member"
      end
    end
  end

  private

  def find_project
    @project = Project.find(params[:project_id])
  end


end
