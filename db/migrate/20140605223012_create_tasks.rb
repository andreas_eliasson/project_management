class CreateTasks < ActiveRecord::Migration
  def change
    create_table :tasks do |t|
      t.text :body
      t.boolean :action
      t.references :project, index: true

      t.timestamps
    end
  end
end
